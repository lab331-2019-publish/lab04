import { Component } from '@angular/core';
import Student from '../../entity/student';

@Component({
  selector: 'app-students-view',
  templateUrl: './students.view.component.html',
  styleUrls: ['./students.view.component.css']
})
export class StudentsViewComponent {
  students: Student[];
}
